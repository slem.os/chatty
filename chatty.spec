Name:		chatty
Version:	0.1.15
Release:	1%{?dist}
Summary:	A libpurple messaging client

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/chatty
%undefine _disable_source_fetch
Source0:        https://source.puri.sm/Librem5/chatty/-/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig(glib-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gobject-2.0) >= 2.50.0
BuildRequires:  libpurple-devel
BuildRequires:  feedbackd-devel
BuildRequires:  libhandy-0_0-0
BuildRequires:  libhandy0-devel
BuildRequires:  sqlite3-devel
BuildRequires:  desktop-file-utils
BuildRequires:  libe-book-devel
BuildRequires:  AppStream
BuildRequires:  AppStream-devel
BuildRequires:  evolution-devel
BuildRequires:  evolution-data-server-devel
BuildRequires:  evolution-data-server
BuildRequires:  eb-devel

%description
Chatty a messaging app and SMS with ModemManager


%prep
%setup -q -n %{name}-v%{version}

%build
%meson
%meson_build


%install
%meson_install


%files
%dir  /etc/xdg/autostart/
/etc/xdg/autostart/sm.puri.Chatty-daemon.desktop
%{_bindir}/chatty
/usr/share/applications/sm.puri.Chatty.desktop
/usr/share/glib-2.0/schemas/sm.puri.Chatty.gschema.xml
/usr/share/icons/hicolor/scalable/apps/sm.puri.Chatty.svg
/usr/share/icons/hicolor/symbolic/apps/sm.puri.Chatty-symbolic.svg
/usr/share/locale/cs/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/de/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/de_DE/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/el/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/en_GB/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/es/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/fi/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/fr/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/hu/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/it/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/it_IT/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/ja/LC_MESSAGES/purism-chatty.mo
%dir /usr/share/locale/la/
%dir /usr/share/locale/la/LC_MESSAGES
/usr/share/locale/la/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/pl/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/pt_BR/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/ro/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/ru/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/sk/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/tr/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/uk/LC_MESSAGES/purism-chatty.mo
/usr/share/locale/da/LC_MESSAGES/purism-chatty.mo
%dir /usr/share/metainfo/
/usr/share/metainfo/sm.puri.Chatty.metainfo.xml
/usr/share/bash-completion/completions/chatty


%changelog
* Fri Sep 18 2020 Adrian Campos Garrido <adriancampos@teachelp.com> - 0.1.15-1
- Update to version 0.1.15

* Sat Jun 27 2020 Adrian Campos <adriancampos@teachelp.com> - 0.1.13-1
- First build
